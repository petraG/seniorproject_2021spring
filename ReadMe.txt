Background
As more people incorporate food production into home life, some are raising backyard fowl as a sustainable and relatively easy way of providing dietary protein. The daily chores that go along with maintaining a healthy flock throughout the year as daylight hours shift can be challenging for those with set work schedules or limited time. To help alleviate some of the stress of these repetitive tasks, I am developing a combined software and hardware system to automate some of the simpler daily activities associated with keeping birds.

This is a senior project for the Champlain College bachelors of science in Software Development. As of spring 2021, this is only in the software stages - hopefully, hardware will follow.

Goals:
    • opening doors after sunrise to allow birds access to outside, daytime area
    • closing doors after dark to minimize nocturnal predation
    • monitoring coop temperature and triggering heat lamp

Stretch Goals:
    • relaying sensor data and system status to mobile device(s)
    • manual overrides of door and heating systems through graphical user interface (GUI)
    • motion sensing for outside area to determine if birds were closed out after dark
    • camera setup for observing flock via mobile device
