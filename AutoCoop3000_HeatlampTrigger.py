'''
Petra Gates
SDEV-435-81: Applied Software Practice I
Spring 2021
Senior Project: Chicken Chore Automation
Module: Heat Lamp Trigger

relay code based on
How to Control a Relay using Raspberry Pi?
by elktros
https://www.electronicshub.org/control-a-relay-using-raspberry-pi/
'''

import RPi.GPIO as GPIO
import time
from datetime import datetime

# accessing get_temp() & device_file
import AutoCoop3000_SensorData as SensData

######################################################
# Python dictionary to hold threshold trigger values #
# for heatlamp and door operation systems            #
######################################################
# Ideally moved to separate module
global thresh_dict
thresh_dict = {'TempThreshold': -6.0, 'AM_Lux_Threshold': 100.0, 'PM_Lux_Threshold': 5.0}

relay_trigger_time = None
heatlamp_on_status = False


def relay_control():
    global relay_trigger_time
    in1 = 16
    in2 = 18
    relay_trigger_time = datetime.datetime.now()

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(in1, GPIO.OUT)
    GPIO.setup(in2, GPIO.OUT)

    GPIO.output(in1, False)
    GPIO.output(in2, False)

    try:
        while True:
            for x in range(5):
                GPIO.output(in1, True)
                time.sleep(0.1)
                GPIO.output(in1, False)
                GPIO.output(in2, True)
                time.sleep(0.1)
                GPIO.output(in2, False)

            GPIO.output(in1, True)
            GPIO.output(in2, True)

            for x in range(4):
                GPIO.output(in1, True)
                time.sleep(0.05)
                GPIO.output(in1, False)
                time.sleep(0.05)
            GPIO.output(in1, True)

            for x in range(4):
                GPIO.output(in2, True)
                time.sleep(0.05)
                GPIO.output(in2, False)
                time.sleep(0.05)
            GPIO.output(in2, True)

    except KeyboardInterrupt:
        GPIO.cleanup()


# Heatlamp triggering function
# (possibly removed, with relay_control being called in GUI)
def heatlamp_trigger():
    global heatlamp_on_status
    if (SensData.get_temp_c(SensData.device_file) <= thresh_dict["TempThreshold"]
      and heatlamp_on_status is False):
        relay_control()
        heatlamp_on_status = True
    elif (SensData.get_temp_c(SensData.device_file) > thresh_dict['TempThreshold']
      and heatlamp_on_status is True):
        relay_control()
        heatlamp_on_status = False


# Heat Lamp Max Duration Cooldown Cycle
def heatlamp_cooldown():
    global relay_trigger_time
    global heatlamp_on_status
    relay_trigger_duration = datetime.datetime.now() - relay_trigger_time
    if (relay_trigger_duration.hours > 6 and heatlamp_on_status is True):
        relay_control()
        heatlamp_on_status = False
        time.sleep(1200)
        relay_control()
        heatlamp_on_status = True
