'''
Petra Gates
SDEV-435-81: Applied Software Practice I
Spring 2021
Senior Project: Chicken Chore Automation
AKA: AutoCoop3000
Module: GUI

PySimpleGUIWeb used to build
https://pysimplegui.readthedocs.io/en/latest/
'''

import PySimpleGUIWeb as sg
import csv
from datetime import datetime
# from AutoCoop3000_HeatlampTrigger import relay_control    # requires RPi
# from AutoCoop300_StepperMotor import door_trigger         # commented for simulation

# Setting PySimpleGUI color theme
sg.theme('LightBlue4')

# Value lists for setting threshold combos (drop-down menus)
temp_thresh_values = [0, -5, -10, -15]
AM_lux_thresh_values = [20, 40, 60, 80]
PM_lux_thresh_values = [20, 15, 10, 5]

# Dictionary to hold sensor threshold values for triggering state changes
thresh_dict = {'TempThreshold': temp_thresh_values[0], 'AM_Lux_Threshold': AM_lux_thresh_values[0], 'PM_Lux_Threshold': PM_lux_thresh_values[0], 'LuxTestThresh': 6000}


########################
# Function Definitions #
########################


def read_from_csv():
    global date_current
    global time_current
    global temp
    global lux
    with open('~/ChickenProjectGit/SensorDataTable.csv', 'r') as csv_file:
        rows = list(csv.reader(csv_file))
        date_current = rows[-1][0]
        time_current = rows[-1][1]
        temp = rows[-1][2]        # access last entered row, temp column
        lux = rows[-1][3]         # access last entered row, lux column


def get_date():
    read_from_csv()
    date = datetime.strptime(date_current, '%Y-%m-%d').date()
    # print("date: ", date)     # for testing
    return date


def get_time():
    read_from_csv()
    time = datetime.strptime(time_current, '%H:%M:%S').time()
    # print("time: ", time)     # for testing
    return time


def get_temp():
    read_from_csv()
    return temp


def get_lamp_status():
    lamp_status = ""
    if (float(temp) <= float(thresh_dict["TempThreshold"])):
        # relay_control()   # commented out for simulation
        lamp_status = "ON"
    else:
        # relay_control()   # commented out for simulation
        lamp_status = "OFF"
    return lamp_status


def get_lux():
    read_from_csv()
    return lux


def get_door_status():
    door_status = "--"
    # should replace dawn/dusk static values with lookup values from sun cycle table
    dawn = datetime(2021, 4, 24, 7, 00, 00).time()
    dusk = datetime(2021, 4, 24, 19, 00, 00).time()
    if (get_time() >= dawn and get_time() <= dusk and float(lux) >= float(thresh_dict["AM_Lux_Threshold"])):
        # door_trigger()        # commented for simulation
        door_status = "OPEN"
    if (get_time() > dusk or get_time() <= dawn and float(lux) <= float(thresh_dict["PM_Lux_Threshold"])):
        # door_trigger()        # commented for simulation
        door_status = "CLOSED"
    # print(door_status)        # for testing
    return door_status


def set_temp_thresh(set_temp):
    thresh_dict["TempThreshold"] = set_temp


def set_AMlux_thresh(set_AM_lux):
    thresh_dict["AM_Lux_Threshold"] = set_AM_lux


def set_PMlux_thresh(set_PM_lux):
    thresh_dict["PM_Lux_Threshold"] = set_PM_lux


# System status fields: labels and sensor readings
list_column1 = [
    # add date and time display
    [sg.Text('Temp C', font=('Arial 18'), size=(16, 1), justification=('center'), background_color='Gray'), sg.Text(get_temp(), font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='temp_key')],
    [sg.Text('Heat Lamp', font=('Arial 18'), size=(16, 1), justification=('center'), background_color='Gray'), sg.Text(get_lamp_status(), font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='lamp_key')],
    [sg.Text('Lux', font=('Arial 18'), size=(16, 1), justification=('center'), background_color='Gray'), sg.Text(get_lux(), font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='lux_key')],
    [sg.Text('Door', font=('Arial 18'), size=(16, 1), justification=('center'), background_color='Gray'), sg.Text(get_door_status(), font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='door_key')]
]

# Page/column: setting sensor thresholds
list_column2 = [
    [sg.Text('Temp Threshold', font=('Arial 18'), size=(16, 1), justification='center', background_color='Gray'), sg.Text(thresh_dict["TempThreshold"], font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='temp_key2')],
    [sg.Button('Set Temp', size=(16, 1)), sg.Combo(values=temp_thresh_values, default_value=temp_thresh_values[0], size=(16, 1), key='set_temp')],
    [sg.Text('AM Lux Threshold', font=('Arial 18'), size=(16, 1), justification='center', background_color='Gray'), sg.Text(thresh_dict["AM_Lux_Threshold"], font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='AMlux_key')],
    [sg.Button('Set AM Lux', size=(16, 1)), sg.Combo(values=AM_lux_thresh_values, default_value=AM_lux_thresh_values[0], size=(16, 1), key='set_AM_lux')],
    [sg.Text('PM Lux Threshold', font=('Arial 18'), size=(16, 1), justification='center', background_color='Gray'), sg.Text(thresh_dict["PM_Lux_Threshold"], font=('Arial 18'), size=(16, 1), justification=('center'), background_color='LightBlue', key='PMlux_key')],
    [sg.Button('Set PM Lux', size=(16, 1)), sg.Combo(values=PM_lux_thresh_values, default_value=PM_lux_thresh_values[0], size=(16, 1), key='set_PM_lux')]
]

# System Status layout
layout1 = [
    [sg.Text(get_date(), font=('Arial 18'), size=(16, 1), justification='center', key='date_key'), sg.Text(get_time(), font=('Arial 18'), size=(16, 1), justification='center', key='time_key')],
    [sg.Column(list_column1)]
]

# Setting Sensor Thresholds layout
layout2 = [
    [sg.Text('Set Sensor Thresholds', font=('Arial 22'), size=(34, 1), justification='center')],
    [sg.Column(list_column2)]
]

layout = [
    [sg.Button('Update System Status')],
    [sg.Column(layout1, key='-COL1-')],
    [sg.Text('')],
    [sg.Column(layout2, key='-COL2-')],
    [sg.Text('Error Messages', font=('Arial 18'), size=(34, 1), justification='center')],
    [sg.Text('', font=('Arial 14'), size=(34, 8), justification='center', background_color=('LightBlue'))],
    [sg.Button('Exit', size=(11, 1))]
]

# Create the window
window = sg.Window('AutoCoop3000', layout, web_ip='127.0.0.1', web_port=8888, web_start_browser=True, element_justification='c')

# Create an event loop
while True:
    event, values = window.read()
    print(event, values)    # testing

    # End program if user closes window
    if event in (None, sg.WIN_CLOSED, 'Exit'):
        break

    # Updating GUI layout on button push: system status/set thresh
    if event == 'Update System Status':
        window['date_key'].update(get_date())
        window['time_key'].update(get_time())
        window['temp_key'].update(get_temp())
        window['lamp_key'].update(get_lamp_status())
        window['lux_key'].update(get_lux())
        window['door_key'].update(get_door_status())
    if event == 'Set Temp':
        set_temp_thresh(values['set_temp'])
        window['temp_key2'].update(values['set_temp'])
    if event == 'Set AM Lux':
        set_AMlux_thresh(values['set_AM_lux'])
        window['AMlux_key'].update(values['set_AM_lux'])
    if event == 'Set PM Lux':
        set_PMlux_thresh(values['set_PM_lux'])
        window['PMlux_key'].update(values['set_PM_lux'])
window.close()


'''
Some Resources:
"PySimpleGUI 2020 Part 7 - Button Targets (File, Calendar, Color Chooser Buttons)""
https://www.youtube.com/watch?v=y7UZUccC2yU

Demo_Table_CSV
https://github.com/PySimpleGUI/PySimpleGUI/blob/master/DemoPrograms/Demo_Table_CSV.py

Demo_Table_Simulation.py
https://github.com/PySimpleGUI/PySimpleGUI/blob/master/DemoPrograms/Demo_Table_Simulation.py

PySimpleGUI Documentation
Keys:
https://pysimplegui.readthedocs.io/en/latest/#keys
Updating Elements:
https://pysimplegui.readthedocs.io/en/latest/#updating-elements-changing-elements-values-in-an-active-window

Multiple Simultaneous Windows
https://github.com/PySimpleGUI/PySimpleGUI/issues/2252

How to display different layouts based on button clicks in PySimple GUI?
https://stackoverflow.com/questions/59500558/how-to-display-different-layouts-based-on-button-clicks-in-pysimple-gui-persis
'''
