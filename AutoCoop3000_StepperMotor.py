'''
Petra Gates
SDEV-435-81:    Applied Software Practice I
Spring 2021
Senior Project: Chicken Chore Automation
Module:         Stepper Motor Code

stepper motor code based on
Raspberry Pi Stepper Motor Tutorial
by Rototron
https://www.rototron.info/raspberry-pi-stepper-motor-tutorial/
'''

from time import sleep
import RPi.GPIO as GPIO


DIR = 20   # Direction GPIO Pin
STEP = 21  # Step GPIO Pin
CW = 1     # Clockwise Rotation
CCW = 0    # Counterclockwise Rotation
SPR = 48   # Steps per Revolution (360 / 7.5)


# Variables for Door Operation
door_is_open = None             # boolean for door motor state
max_step_count = SPR / 3        # this is 16 steps, a third of a revolution
step_position = 0               # track current step postion
delay = .0208
contact_sens_closed = True      # boolean to hold contact sensor state
contact_sig = 1                 # value representing contact sensor output


# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)
GPIO.setup(DIR, GPIO.OUT)
GPIO.setup(STEP, GPIO.OUT)


# clockwise rotation - opening
def open_door():
    GPIO.output(DIR, CW)
    for x in range(max_step_count):
        GPIO.output(STEP, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        sleep(delay)
        global step_position
        step_position += 1
    GPIO.cleanup()


# counterclockwise rotation - closing
def close_door():
    GPIO.output(DIR, CCW)
    for x in range(max_step_count):
        GPIO.output(STEP, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        sleep(delay)
        global step_position
        step_position -= 1
    GPIO.cleanup()


def report_contact_state():
    global contact_sens_closed
    global contact_sig
    if (contact_sig != 1):            # contact_sig == 1 stands in for contact
        contact_sens_closed = False
    else:
        contact_sens_closed = True
    return contact_sens_closed

def door_trigger():
    if (contact_sens_closed is True):
        open_door()
    else:
        close_door()


# setting door_is_open boolean, based on position of stepper motor
def door_open_state():
    global step_position
    if (step_position == 0 and report_contact_state() is True):
        return False
    elif (step_position > 11 and step_position < 17 and report_contact_state() is False):   # 1/4 to 1/3 full rotation
        return True
    else:
        open_door()
        print("Stepper motor system error, door opened.")
