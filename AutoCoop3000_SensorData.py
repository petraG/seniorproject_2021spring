'''
Petra Gates
SDEV-435-81: Applied Software Practice I
Spring 2021
Senior Project: Chicken Chore Automation
Module: Sensor Data collection
        and export to CSV file


temp sensor reading code based on code
Temperature sensor with Raspberry Pi (DS18B20)
by Sashreek Shankar
from: https://iot4beginners.com/temperature-sensor-with-raspberry-pi-ds18b20/
and
Raspberry Pi DS18B20 Temperature Sensor Tutorial
by Scott Campbell
https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

sensor to CSV code based on code by geektechdude
https://github.com/geektechdude/temperature_project/blob/master/write_readings_to_csv
'''

###########
# imports #
###########
from datetime import datetime, timedelta, time
# import glob       # import for hardware device addressing
import threading as th
import csv
import pandas as pd
from AutoCoop3000_HeatlampTrigger import heatlamp_cooldown

##############################################################
# Variables to Produce Simulated Sensor Reading Fluctuations #
##############################################################
temp_val_dropping = True
temp_current = 0
lux_val_dropping = True
lux_current = 120           # Lux value of 120 represents a dark day
datetime_current = datetime(2021, 4, 25, 11, 00, 00)

###########################################################
# Variable holding local location of sensor readings file #
###########################################################
csv_file = "/home/petrag/Documents/Champlain/SDEV-435-81 Applied Software Practice I/ProjectFiles/ChickenProjectGit/SensorDataTable.csv"

# For testing
# csv_file = "/home/petrag/Documents/Champlain/SDEV-435-81 Applied Software Practice I/ProjectFiles/TestDrop.csv"

#############
# Functions #
#############


def read_from_csv():
    global time_current
    with open('~/ChickenProjectGit/SensorDataTable.csv', 'r') as csv_file:
        rows = list(csv.reader(csv_file))
        time_current = rows[-1][1]

    # For testing
   #  with open('/home/petrag/Documents/Champlain/SDEV-435-81 Applied Software Practice I/ProjectFiles/TestDrop.csv') as csv_file:
        # rows = list(csv.reader(csv_file))
        # time_current = rows[-1][1]


def get_time():
    read_from_csv()
    time = datetime.strptime(time_current, '%H:%M:%S').time()
    # print("time: ", time)     # for testing
    return time


def get_temp_c():                  # arg "device_file" removed for simulation
    '''                            # code commented, simulation substituted
    f = open(device_file, "r")
    contents = f.readlines()
    f.close()
    index = contents[-1].find("t=")
    if index != -1:
        temperature = contents[-1][index + 2:]
        temp_c = float(temperature) / 1000
        return temp_c
    '''
    ######################################
    # Simulated Temperature Fluctuations #
    ######################################
    global temp_val_dropping
    global temp_current
    temp_high = -1
    temp_low = -23
    if (temp_val_dropping is True):
        if (temp_current <= temp_low):
            temp_val_dropping = False
        else:
            temp_current -= 2
    else:
        if (temp_current >= temp_high):
            temp_val_dropping = True
        else:
            temp_current += 2
    # print(temp_current)   # For Testing
    return temp_current


'''
def get_temp_f():
    temp_f = get_temp_c() * 9.0 / 5.0 + 32.0      # device_file argument here
    return temp_f
'''


def get_lux():
    '''
    # code commented, simulation substituted
    lux = tsl2591.get_lux()
    for x in range(1, 5):
        x_lux = lux
        time.sleep(0.5)
    lux_rounded = round(x_lux, 2)
    lux_str = str(lux_rounded)
    return(lux_str)
    '''
    ##############################
    # Simulated Lux Fluctuations #
    ##############################
    global lux_val_dropping
    global lux_current
    lux_high = 120          # Lux value of 120 represents a dark day
    lux_low = 1
    if (get_time() <= time(19, 00, 00) and get_time() >= time(7, 00, 00)):
        if (lux_val_dropping is True):
            if (lux_current <= lux_low):
                lux_val_dropping = False
            else:
                lux_current -= 20
        else:
            if (lux_current >= lux_high):
                lux_val_dropping = True
            else:
                lux_current += 20
    else:
        lux_current = 2
        lux_val_dropping = False
    # print(lux_current)          # for testing
    return lux_current


####################################################
# code to simulate datetime cycling for simulation #
####################################################
def date_cycle():
    global datetime_current
    date_current = datetime_current.date()
    return date_current


def time_cycle():
    global datetime_current
    datetime_current = datetime_current + timedelta(hours=1)
    time_current = datetime_current.time()
    # date_current = datetime_current.strftime("%Y-%m-%d")
    # time_current = datetime_current.strftime("%H:%M:%S")
    return time_current


###################################################
# Date & Time functions for non-simulated version #
###################################################
'''
def get_date():
    today = datetime.datetime.now().strftime("%Y-%m-%d")
    return today


def get_time():
    now = datetime.datetime.now().strftime("%H:%M:%S")
    return now
'''


def write_to_csv():
    # mode='a' appends to existing file
    with open(csv_file, mode='a') as sensor_readings:
        sensor_write = csv.writer(
            sensor_readings, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
        write_to_log = sensor_write.writerow(
            ##########################################
            # commented line for non-simulated input #
            ##########################################
            #[get_date(), get_time(), get_temp_c(), get_lux()])
            [date_cycle(), time_cycle(), get_temp_c(), get_lux()])
        return(write_to_log)


# function to drop rows in csv file
def csv_line_limiter():
    file = pd.read_csv(csv_file)        # pandas method to read csv
    # print("len(file): ", len(file))     # testing len=number of rows
    if len(file) > 120:
        print("...CSV file length greater than threshold - trimming...")
        half_rows = int(len(file) / 2)
        ###############################################
        # remove rows 3 (index 2) through len(file)/2 #
        ###############################################
        file = file.drop(file.index[2:half_rows])
        file.to_csv(csv_file, index=False)


# function to periodically write sensor output to csv file
def sensor_data_stream():
    write_to_csv()
    th.Timer(1, sensor_data_stream).start()
    print("sensor_data_stream() timer started")


# function to periodically cull older rows in csv file
def csv_cleanup():
    csv_line_limiter()
    th.Timer(5, csv_cleanup).start()   # set time to reasonable value
    print("csv_cleanup() timer started")


def main_func():
    # instantiating thread objects
    sensor_thread = th.Thread(target=sensor_data_stream)
    csv_thread = th.Thread(target=csv_cleanup)

    # starting thread objects
    sensor_thread.start()
    print("sensor_data_stream() thread started")
    csv_thread.start()
    print("csv_cleanup() thread started")

    # joining thread objects
    sensor_thread.join()
    print("sensor_data_stream() thread joined")
    csv_thread.join()
    print("csv_cleanup() thread joined")


if __name__ == "__main__":
    main_func()

# commenting out hardware related code - replaced with simulation funcs
#######################################################################
'''
# lux sensor & System Management Bus module imports
from python_tsl2591 import tsl2591
try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus

if needed, also
bus=SMBus(1)
tsl25911 = TSL25911(i2c_dev=bus)
(tsl25911 does use i2c)
see:
https://www.waveshare.com/wiki/TSL25911_Light_Sensor

from: https://github.com/LeivoSepp/Lesson-LightSensor-TSL2591
I2C address, "... is fixed to 0x29"

from: https://www.adafruit.com/product/1980
Adafruit TSL2591 uses both 0x29 and 0x28 (I2C addresses)
'''

'''
# sets up the device_file variable for sensor access
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
# device_file = '/sys/bus/w1/devices/28-0314977912af/w1_slave'  # examp sens serial num
'''
